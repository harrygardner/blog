json.array!(@posts) do |post|
  json.extract! post, :id, :Title, :body
  json.url post_url(post, format: :json)
end
