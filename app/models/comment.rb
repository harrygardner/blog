class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_ :post_id
	validates_presence_ :body
end
